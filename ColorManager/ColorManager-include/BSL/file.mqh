/*
Copyright 2019 FXcoder

This file is part of ColorManager.

ColorManager is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ColorManager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with ColorManager. If not, see
http://www.gnu.org/licenses/.
*/

// Класс доступа к файлу. Better Standard Library. © FXcoder
// Файл через этот класс нельзя явно закрыть и переоткрыть. Открытие файла происходит при объявлении или создании через new,
// закрытие при уничтожении объекта (delete или при выходе за область видимости).

#property strict

#include "type/uncopyable.mqh"


class CBFile: public CBUncopyable
{
protected:

	const string path_;
	int handle_;
	int flags_;


public:

	//TODO: варианты с разделителем и кодировкой, вероятно понадобится свойство для режима открытия (вариант перегрузки FileOpen)
	void CBFile(string path):
		path_(path), handle_(INVALID_HANDLE)
	{
	}

	void ~CBFile()
	{
		// не убирать, т.к. от этого зависит другой код
		if (is_open())
			close();
	}

	virtual bool open(int flags)
	{
		flags_ = flags;
		handle_ = ::FileOpen(path_, flags);
		return(is_open());
	}


	bool is_open() const
	{
		return(handle_ != INVALID_HANDLE);
	}


	/* Стандартные функции */

	// Закрытие файла.
	// Файл также принудительно закрывается при удалении этого объекта.
	void close()
	{
		::FileClose(handle_);
	}

	// Сброс на диск всех данных, оставшихся в файловом буфере ввода-вывода.
	void flush()
	{
		::FileFlush(handle_);
	}

	//TODO: разобраться с дублированием с FILE_SIZE
	// Возвращает размер файла в байтах.
	//virtual ulong size() const
	//{
	//	return(FileSize(handle_));
	//}

	// Определяет конец файла в процессе чтения.
	bool is_ending() const
	{
		return(::FileIsEnding(handle_));
	}

	// Определяет конец строки в текстовом файле в процессе чтения.
	bool is_line_ending() const
	{
		return(::FileIsLineEnding(handle_));
	}

	bool seek(long offset, ENUM_FILE_POSITION origin) const
	{
		return(::FileSeek(handle_, offset, origin));
	}

	ulong tell() const
	{
		return(::FileTell(handle_));
	}

	// Свойства

	bool     exists      () const { return( (bool)     ::FileGetInteger( handle_, FILE_EXISTS      )); } // Проверка на существование //todo: проверить. возможно, возвращает -1, как для варианта с путём
	datetime create_date () const { return( (datetime) ::FileGetInteger( handle_, FILE_CREATE_DATE )); } // Дата создания
	datetime modify_date () const { return( (datetime) ::FileGetInteger( handle_, FILE_MODIFY_DATE )); } // Дата последнего изменения
	datetime access_date () const { return( (datetime) ::FileGetInteger( handle_, FILE_ACCESS_DATE )); } // Дата последнего доступа к файлу
	long     size        () const { return( (long)     ::FileGetInteger( handle_, FILE_SIZE        )); } // Размер файла в байтах
	long     position    () const { return( (long)     ::FileGetInteger( handle_, FILE_POSITION    )); } // Позиция указателя в файле
	bool     end         () const { return( (bool)     ::FileGetInteger( handle_, FILE_END         )); } // Получение признака конца файла
	bool     line_end    () const { return( (bool)     ::FileGetInteger( handle_, FILE_LINE_END    )); } // Получение признака конца строки
	bool     is_common   () const { return( (bool)     ::FileGetInteger( handle_, FILE_IS_COMMON   )); } // Файл открыт в общей папке всех клиентских терминалов (смотри FILE_COMMON)
	bool     is_text     () const { return( (bool)     ::FileGetInteger( handle_, FILE_IS_TEXT     )); } // Файл открыт как текстовый (смотри FILE_TXT)
	bool     is_binary   () const { return( (bool)     ::FileGetInteger( handle_, FILE_IS_BINARY   )); } // Файл открыт как бинарный (смотри FILE_BIN)
	bool     is_csv      () const { return( (bool)     ::FileGetInteger( handle_, FILE_IS_CSV      )); } // Файл открыт как CSV (смотри FILE_CSV)
	bool     is_ansi     () const { return( (bool)     ::FileGetInteger( handle_, FILE_IS_ANSI     )); } // Файл открыт как ANSI (смотри FILE_ANSI)
	bool     is_readable () const { return( (bool)     ::FileGetInteger( handle_, FILE_IS_READABLE )); } // Файл открыт с возможностью чтения (смотри FILE_READ)
	bool     is_writable () const { return( (bool)     ::FileGetInteger( handle_, FILE_IS_WRITABLE )); } // Файл открыт с возможностью записи (смотри FILE_WRITE)
};
