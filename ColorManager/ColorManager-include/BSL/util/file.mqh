/*
Copyright 2019 FXcoder

This file is part of ColorManager.

ColorManager is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ColorManager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with ColorManager. If not, see
http://www.gnu.org/licenses/.
*/

// Функции ввода-вывода. Better Standard Library. © FXcoder
//todo: escape_path (пока только windows)
//todo: вынести из util, но потребуется смена имени файла и/или класса у file.mqh на новом месте

#property strict

#include "../type/uncopyable.mqh"
#include "../filetext.mqh"

class CBFileUtil: public CBUncopyable
{
public:

	/*
	Считать текстовый файл по строкам в массив строк.
	@param path       Путь до файла.
	@param lines[]    Ссылка на массив строк, в который будут загружены строки файла.
	@param flags      Дополнительные флаги для FileOpen.
	@return           Количество загруженных строк (размер массива strings[]) или -1 в случае ошибки.
	*/
	static int read_lines(string path, int flags, string &lines[])
	{
		CBFileText file(path);

		if (!file.open(FILE_READ | FILE_TXT | flags))
			return(-1);
			
		return(file.read_lines(lines));
	}

	/*
	Сохранить массив строк в текстовый файл построчно.
	@param path       Путь до файла.
	@param strings[]  Ссылка на массив строк, которые будут сохранены в файл.
	@param flags      Дополнительные флаги для FileOpen.
	@return           Успех операции.
	*/
	static bool write_lines(string path, int flags, const string &lines[])
	{
		CBFileText file(path);
		
		if (!file.open(FILE_WRITE | FILE_TXT | flags))
			return(false);
		
		return(file.write_lines(lines));
	}

	//todo: проверить все результаты из-за обнаруженной недокументированной хрени в exists и size с exists
	static bool     exists      (const string path, bool common_folder = false) { return( 1 ==       ::FileGetInteger( path, FILE_EXISTS,      common_folder )); } // Проверка на существование (не использовать (bool), т.к. в случае неудачи возвращается -1
	static datetime create_date (const string path, bool common_folder = false) { return( (datetime) ::FileGetInteger( path, FILE_CREATE_DATE, common_folder )); } // Дата создания
	static datetime modify_date (const string path, bool common_folder = false) { return( (datetime) ::FileGetInteger( path, FILE_MODIFY_DATE, common_folder )); } // Дата последнего изменения
	static datetime access_date (const string path, bool common_folder = false) { return( (datetime) ::FileGetInteger( path, FILE_ACCESS_DATE, common_folder )); } // Дата последнего доступа к файлу
	static long     size        (const string path, bool common_folder = false) { return(            ::FileGetInteger( path, FILE_SIZE,        common_folder )); } // Размер файла в байтах (-1, если файла нет)

} _file;
