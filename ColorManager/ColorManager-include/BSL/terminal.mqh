/*
Copyright 2019 FXcoder

This file is part of ColorManager.

ColorManager is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ColorManager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with ColorManager. If not, see
http://www.gnu.org/licenses/.
*/

// Класс доступа к свойстам терминала. Better Standard Library. © FXcoder

#property strict

#include "type/uncopyable.mqh"

class CBTerminal: public CBUncopyable
{
public:

	// функции терминала
	static bool close(int ret_code) { return(::TerminalClose(ret_code)); } // Посылает терминалу команду на завершение работы

	// свойства int/bool
	static int  build                 () { return(        ::TerminalInfoInteger(TERMINAL_BUILD                 )); } // Номер билда запущенного терминала
	static bool community_account     () { return( (bool) ::TerminalInfoInteger(TERMINAL_COMMUNITY_ACCOUNT     )); } // Флаг наличия авторизационных данных MQL5.community в терминале
	static bool community_connection  () { return( (bool) ::TerminalInfoInteger(TERMINAL_COMMUNITY_CONNECTION  )); } // Наличие подключения к MQL5.community
	static int  codepage              () { return(        ::TerminalInfoInteger(TERMINAL_CODEPAGE              )); } // Номер кодовой страницы языка, установленного в клиентском терминале
	static bool connected             () { return( (bool) ::TerminalInfoInteger(TERMINAL_CONNECTED             )); } // Наличие подключения к торговому серверу
	static int  cpu_cores             () { return(        ::TerminalInfoInteger(TERMINAL_CPU_CORES             )); } // Количество процессоров в системе
	static int  disk_space            () { return(        ::TerminalInfoInteger(TERMINAL_DISK_SPACE            )); } // Объем свободной памяти на диске для папки MQL5\Files терминала (агента), MB
	static bool dlls_allowed          () { return( (bool) ::TerminalInfoInteger(TERMINAL_DLLS_ALLOWED          )); } // Разрешение на использование DLL
	static bool email_enabled         () { return( (bool) ::TerminalInfoInteger(TERMINAL_EMAIL_ENABLED         )); } // Разрешение на отправку писем с использованием SMTP-сервера и логина, указанных в настройках терминала
	static bool ftp_enabled           () { return( (bool) ::TerminalInfoInteger(TERMINAL_FTP_ENABLED           )); } // Разрешение на отправку отчетов по FTP на указанный сервер для указанного в настройках терминала торгового счета
	static int  max_bars              () { return(        ::TerminalInfoInteger(TERMINAL_MAXBARS               )); } // Максимальное количество баров на графике
	static int  memory_available      () { return(        ::TerminalInfoInteger(TERMINAL_MEMORY_AVAILABLE      )); } // Размер свободной памяти процесса терминала (агента), MB
	static int  memory_physical       () { return(        ::TerminalInfoInteger(TERMINAL_MEMORY_PHYSICAL       )); } // Размер физической памяти в системе, MB
	static int  memory_total          () { return(        ::TerminalInfoInteger(TERMINAL_MEMORY_TOTAL          )); } // Размер памяти, доступной процессу терминала (агента), MB
	static int  memory_used           () { return(        ::TerminalInfoInteger(TERMINAL_MEMORY_USED           )); } // Размер памяти, использованной терминалом (агентом), MB
	static bool mqid                  () { return( (bool) ::TerminalInfoInteger(TERMINAL_MQID                  )); } // Флаг наличия MetaQuotes ID для отправки Push-уведомлений
	static bool notifications_enabled () { return( (bool) ::TerminalInfoInteger(TERMINAL_NOTIFICATIONS_ENABLED )); } // Разрешение на отправку уведомлений на смартфон
	static int  onencl_support        () { return(        ::TerminalInfoInteger(TERMINAL_OPENCL_SUPPORT        )); } // Версия поддерживаемой OpenCL в виде 0x00010002 = 1.2.  "0" означает, что OpenCL не поддерживается
	static int  ping_last             () { return(        ::TerminalInfoInteger(TERMINAL_PING_LAST             )); } // Последнее известное значение пинга до торгового сервера в микросекундах. В одной секунде миллион микросекунд
	static int  screen_dpi            () { return(        ::TerminalInfoInteger(TERMINAL_SCREEN_DPI            )); } // Разрешающая способность вывода информации на экран (DPI)
	static bool trade_allowed         () { return( (bool) ::TerminalInfoInteger(TERMINAL_TRADE_ALLOWED         )); } // Разрешение на торговлю
	static bool x64                   () { return( (bool) ::TerminalInfoInteger(TERMINAL_X64                   )); } // Признак "64 битный терминал"

	// клавиши
	static int key_state_left       () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_LEFT     )); }
	static int key_state_up         () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_UP       )); }
	static int key_state_right      () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_RIGHT    )); }
	static int key_state_down       () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_DOWN     )); }
	static int key_state_shift      () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_SHIFT    )); }
	static int key_state_control    () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_CONTROL  )); }
	static int key_state_menu       () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_MENU     )); }
	static int key_state_capslock   () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_CAPSLOCK )); }
	static int key_state_numlock    () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_NUMLOCK  )); }
	static int key_state_scrlock    () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_SCRLOCK  )); }
	static int key_state_enter      () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_ENTER    )); }
	static int key_state_insert     () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_INSERT   )); }
	static int key_state_delete     () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_DELETE   )); }
	static int key_state_home       () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_HOME     )); }
	static int key_state_end        () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_END      )); }
	static int key_state_tab        () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_TAB      )); }
	static int key_state_pageup     () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_PAGEUP   )); }
	static int key_state_pagedown   () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_PAGEDOWN )); }
	static int key_state_escape     () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_ESCAPE   )); }

	// свойства double
	static double community_balance () { return(::TerminalInfoDouble(TERMINAL_COMMUNITY_BALANCE)); } // Баланс пользователя в MQL5.community

	// свойства string
	static string language          () { return(::TerminalInfoString(TERMINAL_LANGUAGE        )); } // Язык терминала
	static string company           () { return(::TerminalInfoString(TERMINAL_COMPANY         )); } // Имя компании
	static string name              () { return(::TerminalInfoString(TERMINAL_NAME            )); } // Имя терминала
	static string path              () { return(::TerminalInfoString(TERMINAL_PATH            )); } // Папка, из которой запущен терминал
	static string data_path         () { return(::TerminalInfoString(TERMINAL_DATA_PATH       )); } // Папка, в которой хранятся данные терминала
	static string common_data_path  () { return(::TerminalInfoString(TERMINAL_COMMONDATA_PATH )); } // Общая папка всех клиентских терминалов, установленных на компьютере

	// Дополнительные функции и свойства

	// клавиши
	static bool is_left_key_pressed       () { return(is_key_pressed(TERMINAL_KEYSTATE_LEFT     )); }
	static bool is_up_key_pressed         () { return(is_key_pressed(TERMINAL_KEYSTATE_UP       )); }
	static bool is_right_key_pressed      () { return(is_key_pressed(TERMINAL_KEYSTATE_RIGHT    )); }
	static bool is_down_key_pressed       () { return(is_key_pressed(TERMINAL_KEYSTATE_DOWN     )); }
	static bool is_shift_key_pressed      () { return(is_key_pressed(TERMINAL_KEYSTATE_SHIFT    )); }
	static bool is_control_key_pressed    () { return(is_key_pressed(TERMINAL_KEYSTATE_CONTROL  )); }
	static bool is_menu_key_pressed       () { return(is_key_pressed(TERMINAL_KEYSTATE_MENU     )); }
	static bool is_capslock_key_pressed   () { return(is_key_pressed(TERMINAL_KEYSTATE_CAPSLOCK )); }
	static bool is_numlock_key_pressed    () { return(is_key_pressed(TERMINAL_KEYSTATE_NUMLOCK  )); }
	static bool is_scrlock_key_pressed    () { return(is_key_pressed(TERMINAL_KEYSTATE_SCRLOCK  )); }
	static bool is_enter_key_pressed      () { return(is_key_pressed(TERMINAL_KEYSTATE_ENTER    )); }
	static bool is_insert_key_pressed     () { return(is_key_pressed(TERMINAL_KEYSTATE_INSERT   )); }
	static bool is_delete_key_pressed     () { return(is_key_pressed(TERMINAL_KEYSTATE_DELETE   )); }
	static bool is_home_key_pressed       () { return(is_key_pressed(TERMINAL_KEYSTATE_HOME     )); }
	static bool is_end_key_pressed        () { return(is_key_pressed(TERMINAL_KEYSTATE_END      )); }
	static bool is_tab_key_pressed        () { return(is_key_pressed(TERMINAL_KEYSTATE_TAB      )); }
	static bool is_pageup_key_pressed     () { return(is_key_pressed(TERMINAL_KEYSTATE_PAGEUP   )); }
	static bool is_pagedown_key_pressed   () { return(is_key_pressed(TERMINAL_KEYSTATE_PAGEDOWN )); }
	static bool is_escape_key_pressed     () { return(is_key_pressed(TERMINAL_KEYSTATE_ESCAPE   )); }


#ifndef __MQL4__

	// Свойства

	// 5.1730
	static double retransmission  () { return(::TerminalInfoDouble(TERMINAL_RETRANSMISSION)); } // Процент повторно отправляемых сетевых пакетов в TCP/IP протоколе для всех запущенных приложений и служб на данном компьютере

	// 5.1930
	static int screen_left   () { return(::TerminalInfoInteger(TERMINAL_SCREEN_LEFT  )); } // Левая координата виртуального экрана
	static int screen_top    () { return(::TerminalInfoInteger(TERMINAL_SCREEN_TOP   )); } // Верхняя координата виртуального экрана
	static int screen_width  () { return(::TerminalInfoInteger(TERMINAL_SCREEN_WIDTH )); } // Ширина терминала?
	static int screen_height () { return(::TerminalInfoInteger(TERMINAL_SCREEN_HEIGHT)); } // Высота терминала?

	static int left   () { return(::TerminalInfoInteger(TERMINAL_LEFT  )); } // Левая координата терминала относительно виртуального экрана
	static int top    () { return(::TerminalInfoInteger(TERMINAL_TOP   )); } // Верхняя координата терминала относительно виртуального экрана
	static int right  () { return(::TerminalInfoInteger(TERMINAL_RIGHT )); } // Правая координата терминала относительно виртуального экрана
	static int bottom () { return(::TerminalInfoInteger(TERMINAL_BOTTOM)); } // Нижняя координата терминала относительно виртуального экрана

	// 5.2007
	static bool vps   () { return((bool)::TerminalInfoInteger(TERMINAL_VPS)); } // Признак того, что терминал запущен на MetaTrader VPS

#endif


protected:

	// Проверить, нажата ли клавиша (не путать с включенной, например клавиши *Lock, они проверяются по младшему биту)
	static bool is_key_pressed(ENUM_TERMINAL_INFO_INTEGER key) { return((::TerminalInfoInteger(key) & 0x80) != 0); }
		
} _terminal;
