> **Этот проект закрыт. Обновления будут выходить только для MetaTrader 5 в новом проекте: [ColorManager-MT5](https://gitlab.com/fxcoder-mql/colormanager-mt5).**

# Скрипт ColorManager

Скрипт позволяет управлять цветовыми схемами графика. Вместо стандартных трёх-четырёх схем можно использовать намного больше своих.

![](media/colormanager_main_480x320.png)

Схемы хранятся в файле MQL/Files/ColorManager/colors.ini терминала ([узнать расположение папки MQL4/5](https://www.metatrader5.com/ru/metaeditor/help/beginning/open)).

## См. также

В блоге: <https://www.fxcoder.ru/search/label/%7BColorManager%7D>

Старую версию можно взять здесь: [https://gitlab.com/fxcoder/mt-script-archive](https://gitlab.com/fxcoder/mt-script-archive).

Установка: <https://www.fxcoder.ru/p/install-script.html>

:flag_gb:

Install: <https://www.fxcoder.ru/p/install-script-en.html>
